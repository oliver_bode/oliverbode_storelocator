<?php
namespace Oliverbode\Storelocator\Block\Widget;

class StorelocatorList extends \Oliverbode\Storelocator\Block\StorelocatorList implements \Magento\Widget\Block\BlockInterface
{
    public function getTemplate()
    {
        if (is_null($this->_template)) {
            $this->_template = 'Oliverbode_Storelocator::list.phtml';
        }
        return $this->_template;
    }
}
