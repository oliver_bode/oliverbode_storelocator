<?php
namespace Oliverbode\Storelocator\Block\Adminhtml\Storelocator\Edit;

/**
 * Adminhtml storelocator edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Directory\Model\Config\Source\Country $countryFactory,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_countryFactory = $countryFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('storelocator_form');
        $this->setTitle(__('Store Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Oliverbode\Storelocator\Model\Storelocator $model */
        $model = $this->_coreRegistry->registry('storelocator_storelocator');


        /** @var \Magento\Framework\Data\Form $form */
        /**
         * @todo check how method works maybe it should be storelocator most likely just method type
         */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data']]
        );

        $form->setHtmlIdPrefix('storelocator_');

        $countries = $this->_countryFactory->toOptionArray();

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

//        $fieldset->addType('image', '\Oliverbode\Storelocator\Block\Adminhtml\Storelocator\Helper\Image');

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'store_title',
            'text',
            ['name' => 'store_title', 'label' => __('Store Title'), 'title' => __('Store Title'), 'required' => true]
        );

        $fieldset->addField(
            'address',
            'text',
            [
                'name' => 'address',
                'label' => __('Address'),
                'title' => __('Address'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'city',
            'text',
            [
                'name' => 'city',
                'label' => __('City'),
                'title' => __('City'),
                'required' => true,
            ]
        );
        
        $fieldset->addField(
            'state',
            'select',
            [
                'name' => 'state',
                'label' => __('State'),
                'title' => __('State'),
                'values' =>  ['--Please Select--'],
            ]
        );
        $country = $fieldset->addField(
            'country',
            'select',
            [
                'name' => 'country',
                'label' => __('Country'),
                'title' => __('Country'),
                'values' => $countries,
            ]
        );
        $country->setAfterElementHtml("   
            <script type=\"text/javascript\">
                    require([
                    'jquery',
                    'mage/template',
                    'jquery/ui',
                    'mage/translate'
                ],
                function($, mageTemplate) {
                   var selectedCountry = $('#storelocator_country').val();
                   setTimeout(function() { $('#storelocator_country').val(selectedCountry).trigger('change'); },100);
                   $('#edit_form').on('change', '#storelocator_country', function(event){
                        $.ajax({
                               url : '". $this->getUrl('storelocator/stores/regionlist') . "country/' +  $('#storelocator_country').val() + '/state/" . $model->load($this->getRequest()->getParam('id'))->getState() . "',
                                type: 'POST',
                                data: {form_key: window.FORM_KEY},
                                dataType: 'json',
                               showLoader:true,
                               success: function(data){
                                    $('#storelocator_state').empty();
                                    $('#storelocator_state').append(data.htmlconent);
                               }
                            });
                   })
                }

            );
            </script>"
        );

        $fieldset->addField(
            'postcode',
            'text',
            [
                'name' => 'postcode',
                'label' => __('Postcode'),
                'title' => __('Postcode'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'phone',
            'text',
            [
                'name' => 'phone',
                'label' => __('Phone'),
                'title' => __('Phone'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'email',
            'text',
            [
                'name' => 'email',
                'label' => __('Email'),
                'title' => __('Email'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'lat',
            'text',
            [
                'name' => 'lat',
                'label' => __('Lat'),
                'title' => __('Lat'),
            ]
        );

        $fieldset->addField(
            'lng',
            'text',
            [
                'name' => 'lng',
                'label' => __('Lng'),
                'title' => __('Lng'),
            ]
        );

        $fieldset->addField(
            'image',
            'image',
            [
                'name' => 'image',
                'label' => __('Image'),
                'title' => __('Image'),
            ]
        );

        $fieldset->addField(
            'content',
            'textarea',
            [
                'name' => 'content',
                'label' => __('Content'),
                'title' => __('Content'),
            ]
        );

        $fieldset->addField(
            'is_enable',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_enable',
                'required' => true,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );

        if (!$model->getId()) {
            $model->setData('is_enable', '1');
        }

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
