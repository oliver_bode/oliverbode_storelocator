<?php
namespace Oliverbode\Storelocator\Block\Adminhtml\Storelocator;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize storelocator edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Oliverbode_Storelocator';
        $this->_controller = 'adminhtml_storelocator';

        parent::_construct();

        if ($this->_isAllowedAction('Oliverbode_Storelocator::save')) {
            $this->buttonList->update('save', 'label', __('Save Store'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }

        if ($this->_isAllowedAction('Oliverbode_Storelocator::storelocator_delete')) {
            $this->buttonList->update('delete', 'label', __('Delete Store'));
        } else {
            $this->buttonList->remove('delete');
        }
    }

    /**
     * Retrieve text for header element depending on loaded storelocator
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('storelocator_storelocator')->getId()) {
            return __("Edit Store '%1'", $this->escapeHtml($this->_coreRegistry->registry('storelocator_storelocator')->getStoreTitle()));
        } else {
            return __('New Store');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        /**
         * @todo check this url is correct
         */
        return $this->getUrl('storelocator/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
}
