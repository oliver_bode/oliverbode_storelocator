<?php
namespace Oliverbode\Storelocator\Block;

use Magento\Framework\View\Element\Template\Context;
use Oliverbode\Storelocator\Model\ResourceModel\Storelocator\CollectionFactory;
use Oliverbode\Storelocator\Helper\Data;

class StorelocatorContent extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{

    /**
     * @var \Magento\Framework\View\Element\Template\Context
     */
    protected $context;

    /**
     * @var \Oliverbode\Storelocator\Model\ResourceModel\Storelocator\CollectionFactory
     */
    protected $storelocatorCollectionFactory;

    /**
     * @var \Oliverbode\Storelocator\Helper\Data
     */
    protected $imageHelper;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Oliverbode\Storelocator\Model\ResourceModel\Faq\CollectionFactory $faqCollectionFactory,
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $imageHelper,
        CollectionFactory $storelocatorCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_storelocatorCollectionFactory = $storelocatorCollectionFactory;
        $this->imageHelper = $imageHelper;
    }

    public function getStores()
    {
        if (!$this->hasData('storelocator')) {
            $storelocator = $this->_storelocatorCollectionFactory
                ->create()
                ->addFilter('is_enable', 1)
                ->setOrder('store_title','ASC');
            $this->setData('storelocator', $storelocator);
        }
        return $this->getData('storelocator');
    }

    public function getStoresJson() {
        $array = array();
        $i = 0;
        foreach($this->getStores() as $store) {
            foreach($store->getData() as $key => $value) {
                if ($key == 'image' && !$value) {
                    $value = $this->getImage();
                }
                else if ($key == 'image') {
                    $value = $this->getMediaUrl() . $value;   
                }
                $array[$i][$key] = $value;
            }
            $i++;
        }
        return json_encode($array);
    }



    public function getStyle() {
        return $this->getValue('storelocator/general/styles', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
    }

    public function getGoogleApiUrl() {
        $apiUrl = $this->getValue('storelocator/general/apiurl', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if(is_null($apiUrl)) $apiUrl = "http://maps.googleapis.com/maps/api/js?v=3";
        $apiKey = "&key=". $this->getAPIKey();
        $urlGoogleApi = $apiUrl."&sensor=false".$apiKey."&callback=initialize&libraries=places";
        return $urlGoogleApi;
    }

    public function getAPIKey() {
        return $this->getValue('storelocator/general/apikey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
    }

    public function defaultImageSet() {
        return $this->getValue('storelocator/general/image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPathMarker() {
        return $this->getMediaUrl();
    }

    public function getMediaUrl() {
        return $this->imageHelper->getBaseUrl() . $this->imageHelper::MEDIA_PATH . '/';
    }

    public function getDefaultMarker() {
        return $this->getValue('storelocator/general/marker', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
    }

    public function getImage() {
        return $this->getMediaUrl() . $this->getValue('storelocator/general/store_image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
    }

    public function getDirection() {
        return $this->getValue('storelocator/general/directions', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
    }


    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Oliverbode\Storelocator\Model\Storelocator::CACHE_TAG . '_' . 'content'];
    }
}
