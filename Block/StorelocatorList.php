<?php
namespace Oliverbode\Storelocator\Block;

use Oliverbode\Storelocator\Api\Data\StorelocatorInterface;
use Oliverbode\Storelocator\Model\ResourceModel\Storelocator\Collection as FaqCollection;

class StorelocatorList extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{
    /**
     * @var \Oliverbode\Storelocator\Model\ResourceModel\Faq\CollectionFactory
     */
    protected $_storelocatorCollectionFactory;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Oliverbode\Storelocator\Model\ResourceModel\Faq\CollectionFactory $faqCollectionFactory,
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Oliverbode\Storelocator\Model\ResourceModel\Storelocator\CollectionFactory $storelocatorCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_storelocatorCollectionFactory = $storelocatorCollectionFactory;
    }

    /**
     * @return \Oliverbode\Storelocator\Model\ResourceModel\Faq\Collection
     */
    public function getStores()
    {
        // Check if stores has already been defined
        // makes our block nice and re-usable! We could
        // pass the 'stores' data to this block, with a collection
        // that has been filtered differently!

        /**
         * @todo check sort order is doing as it should
         * May need to be StorelocatorInterface::SORT_ORDER_DESC
         */
        if (!$this->hasData('storelocator')) {
            $storelocator = $this->_storelocatorCollectionFactory
                ->create()
                ->addFilter('is_enable', 1)
                ->setOrder('store_title','ASC');
            $this->setData('storelocator', $storelocator);
        }
        return $this->getData('storelocator');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Oliverbode\Storelocator\Model\Storelocator::CACHE_TAG . '_' . 'list'];
    }

}
