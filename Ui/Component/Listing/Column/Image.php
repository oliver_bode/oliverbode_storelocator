<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Oliverbode\Storelocator\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
 
class Image extends \Magento\Ui\Component\Listing\Columns\Column
{
    const NAME = 'image';
 
    const ALT_FIELD = 'store_title';
 
 
    /**
     * @var string
     */
    private $editUrl;
 
    private $_objectManager = null;
 
    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Oliverbode\Storelocator\Helper\Data $imageHelper
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Oliverbode\Storelocator\Helper\Data $imageHelper,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->scopeConfig = $scopeConfig;
        $this->imageHelper = $imageHelper;
        $this->urlBuilder = $urlBuilder;
    }
 
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $image = ($item['image']) ? $item['image'] : $this->imageHelper::MEDIA_PATH . '/' . $this->scopeConfig->getValue('storelocator/general/store_image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
                $item[$fieldName . '_src'] = $this->imageHelper->getBaseUrl() . $image;
                $item[$fieldName . '_alt'] = $item['store_title'] ?: $image;
                $item[$fieldName . '_orig_src'] = $this->imageHelper->getBaseUrl() . $image;
            }
        }
        return $dataSource;
    }
}