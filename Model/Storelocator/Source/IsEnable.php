<?php
namespace Oliverbode\Storelocator\Model\Storelocator\Source;

class IsEnable implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Oliverbode\Storelocator\Model\Storelocator
     */
    protected $storelocator;

    /**
     * Constructor
     *
     * @param \Oliverbode\Storelocator\Model\Storelocator $storelocaor
     */
    public function __construct(\Oliverbode\Storelocator\Model\Storelocator $storelocator)
    {
        $this->storelocator = $storelocator;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->storelocator->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
