<?php namespace Oliverbode\Storelocator\Model;

use Oliverbode\Storelocator\Api\Data\StorelocatorInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Storelocator extends \Magento\Framework\Model\AbstractModel implements StorelocatorInterface, IdentityInterface
{


    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'storelocator_storelocator';

    /**
     * @var string
     */
    protected $_cacheTag = 'storelocator_storelocator';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'storelocator_storelocator';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Oliverbode\Storelocator\Model\ResourceModel\Storelocator');
    }

    /**
     * Prepare storelocator's statuses.
     * Available event storelocator_storelocator_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getStoreTitle()
    {
        return $this->getData(self::STORE_TITLE);
    }

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * Get state
     *
     * @return string|null
     */
    public function getState()
    {
        return $this->getData(self::STATE);
    }

    /**
     * Get postcode
     *
     * @return string|null
     */
    public function getPostcode()
    {
        return $this->getData(self::POSTCODE);
    }

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * Get phone
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->getData(self::PHONE);
    }

    /**
     * Get email
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * Get lat
     *
     * @return string|null
     */
    public function getLat()
    {
        return $this->getData(self::LAT);
    }

    /**
     * Get lng
     *
     * @return string|null
     */
    public function getLng()
    {
        return $this->getData(self::LNG);
    }

    /**
     * Get image
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get is_enable
     *
     * @return bool|null
     */
    public function getIsEnable()
    {
        return $this->getData(self::IS_ENABLE);
    }


    /**
     * Set ID
     *
     * @param int $id
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set store_title
     *
     * @param string $storeTitle
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setStoreTitle($storeTitle)
    {
        return $this->setData(self::STORE_TITLE, $storeTitle);
    }


    /**
     * Set address
     *
     * @param string $address
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setaddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }


    /**
     * Set city
     *
     * @param string $sity
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }


    /**
     * Set state
     *
     * @param string $state
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setState($state)
    {
        return $this->setData(self::STATE, $state);
    }


    /**
     * Set postcode
     *
     * @param string $postcode
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setPostcode($postcode)
    {
        return $this->setData(self::POSTCODE, $postcode);
    }


    /**
     * Set country
     *
     * @param string $country
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }


    /**
     * Set phone
     *
     * @param string $phone
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setphone($phone)
    {
        return $this->setData(self::PHONE, $phone);
    }


    /**
     * Set email
     *
     * @param string $email
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }


    /**
     * Set lat
     *
     * @param string $lat
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setLat($lat)
    {
        return $this->setData(self::LAT, $lat);
    }

    /**
     * Set lng
     *
     * @param string $lng
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setLng($lng)
    {
        return $this->setData(self::LNG, $lng);
    }

    /**
     * Set image
     *
     * @param string $image
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }


    /**
     * Set is_enable
     *
     * @param string $isEnable
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setIsEnable($isEnable)
    {
        return $this->setData(self::IS_ENABLE, $isEnable);
    }
}
