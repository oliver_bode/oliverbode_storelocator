<?php
namespace Oliverbode\Storelocator\Model\ResourceModel;

/**
 * Storelocator mysql resource
 */
class Storelocator extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('oliverbode_store_location', 'id');
    }


    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param \Oliverbode\Storelocator\Model\Storelocator $object
     * @return \Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        if ($object->getStoreId()) {

            $select->where(
                'is_enable = ?',
                1
            )->limit(
                    1
                );
        }

        return $select;
    }
}
