<?php

namespace Oliverbode\Storelocator\Model\ResourceModel\Storelocator;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Oliverbode\Storelocator\Model\Storelocator', 'Oliverbode\Storelocator\Model\ResourceModel\Storelocator');
    }
}
