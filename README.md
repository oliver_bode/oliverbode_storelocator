# Magento2 Storelocator #

Help customers locate your stores, dealers, distributors, products or services. Storelocator is user friendly, simple yet flexible geo location and directions service with worldwide location support.

## Features ##
* Well organized, easy to use, professionally designed and customizable interface.
* Custom Map Location Icons. Use Google default location markers or create custom map marker icons.
* Allows for full control of layout and styling with CSS to seamlessly blend with your site's distinct look and feel.
* Provides directions to any defined location
* Easily import locations with CSV
* Ability to assign locations to all or specific stores
* Features fully functional Google Map including custom styles that can be found on https://snazzymaps.com
* Fast Results. Integrated with and powered by the latest version of Google MAP API v3
* Support for worldwide locations ( some Google Map limitations might apply )
* Full support for all major and most popular browsers and Magento versions
* Comes with 12 months of unlimited support by Magescale
* Customization ready - contact us for your specific unique needs

## Install Storelocator ##
### Manual Installation ###

Install Storelocator for Magento2

* Download the extension
* Unzip the file
* Create a folder {Magento root}/app/code/Oliverbode/Storelocator
* Copy the content from the unzip folder

### Composer Installation ###

```
#!

composer config repositories.oliverbode_storelocator vcs https://oliver_bode@bitbucket.org/oliver_bode/oliverbode_storelocator.git
composer require oliverbode/storelocator
```

## Enable Storelocator ##

```
#!

php -f bin/magento module:enable --clear-static-content Oliverbode_Storelocator
php -f bin/magento setup:upgrade
```

## Configure Storelocator ##

Log into your Magetno Admin, then goto Stores -> Configuration -> Storelocator

To add stores in admin goto Content -> Storelocator -> Stores

To import stores in admin gotto Content -> Storelocator -> Import Stores

By default the storelocator will appear on http://your-magento-domain.com/stores