<?php

namespace Oliverbode\Storelocator\Controller\Index;

class Stores extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $storelocatorCollectionFactory;
    const STATUS_ENABLED  = 'Enabled';
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Oliverbode\Storelocator\Model\ResourceModel\Storelocator\Collection $storelocatorCollectionFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->storelocatorCollectionFactory = $storelocatorCollectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $city = $this->getRequest()->getParam('city');


        if ($city != '') {
            $collection =$this->storelocatorCollectionFactory
                               ->addFieldToSelect('*')
                               ->addFieldToFilter('city', $city)
                               ->addFieldToFilter('is_enable', self::STATUS_ENABLED);
            $stores = "<ul class='store-addresses'>";
            foreach ($collection as $_store) {
                if($_store['city']) {
                    $stores .= "<li>";
                    $stores .= "<p>" . $_store['store_title'] . "</p>";
                    $stores .= "<p>" . $_store['address'] . "</p>";
                    $stores .= "<p>" . $_store['city'] . "</p>";
                    $stores .= "<p>" . $_store['state'] . "</p>";
                    $stores .= "<p>" . $_store['postcode'] . "</p>";
                    $stores .= "<p>" . $_store['phone'] . "</p>";
                    $stores .= "<p>" . $_store['email'] . "</p>";
                    $stores .= "<p>" . $_store['lat'] . "</p>";
                    $stores .= "<p>" . $_store['lng'] . "</p>";

                    $stores .= "</li>";

                }
            }
            $stores .="</ul>";
        }

        $result['htmlconent']=$stores;
        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }
}