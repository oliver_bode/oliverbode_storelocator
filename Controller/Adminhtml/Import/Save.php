<?php
namespace Oliverbode\Storelocator\Controller\Adminhtml\Import;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $allowedExtensions = ['csv'];

    protected $fileId = 'file';

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Oliverbode_Storelocator::save');
    }



    public function __construct(
        Action\Context $context
    ) {
        parent::__construct($context);
    }


    public function execute()
    {
        $model = $this->_objectManager->create('Oliverbode\Storelocator\Model\Storelocator');

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $validStores = $this->validateFile($_FILES,$model);
            if (count($validStores)) {
                foreach ($validStores as $store) {
                    if (!$store['lat'] || !$store['lat']) {
                        $address = $store['address'] . ', ' . $store['city'] . ' ' . $store['state'] . ' ' . $store['postcode'] . ' ' . $store['country'];
                        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
                        $geo = json_decode($geo, true);
                        if ($geo['status'] == 'OK') {
                          $store['lat'] = $geo['results'][0]['geometry']['location']['lat'];
                          $store['lng'] = $geo['results'][0]['geometry']['location']['lng'];
                        }
                    }
                    $model->setData($store);
                    $model->save();
                }
                return $resultRedirect->setPath('storelocator/stores/');
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(
                __($e->getMessage())
            );
        }
    }

    function underscoreToCamelCase($string) 
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
        return 'set' . $str;
    }

    
    public function validateFile($filePath,$model)
    {
        $collection = $model->getCollection();
        $keys = array_keys($collection->getFirstItem()->getData());
        array_shift($keys);

        $csv = array_map('str_getcsv', file($_FILES['csv_file']['tmp_name']));
        array_walk($csv, function(&$a) use ($csv) {
          $a = array_combine($csv[0], $a);
        });
        array_shift($csv);
        $stores = [];
        for ($i = 0; $i < count($csv); $i ++) {
            reset ($csv[$i]);
            while (list($key, $val) = each($csv[$i])) {
                if (in_array($key, array('ID', 'Action'))) {
                    unset($csv[$i][$key]);
                }
                elseif ($key == 'Status') $stores[$i]['is_enable'] = $val;
                else $stores[$i][str_replace(' ','_',strtolower($key))] = $val;
            }
        }
        $validStores = array();
        foreach ($stores as $store) {
            $storeKeys = array_keys($store);
            if ($keys === $storeKeys || empty($keys)) {
                $validStores[] = $store;
            }
        }
        if (count($validStores)) foreach ($collection as $data) $data->delete();
        return $validStores;
    }
}
