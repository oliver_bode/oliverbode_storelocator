<?php
namespace Oliverbode\Storelocator\Controller\Adminhtml\Import;

use Magento\Backend\App\Action;

class Index extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Oliverbode_Storelocator::save');
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    /**
     * Edit Storelocator
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // $id = $this->getRequest()->getParam('id');
        // $model = $this->_objectManager->create('Oliverbode\Storelocator\Model\Storelocator');

        // if ($id) {
        //     $model->load($id);
        //     if (!$model->getId()) {
        //         $this->messageManager->addError(__('This Store no longer exists.'));
        //         /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        //         $resultRedirect = $this->resultRedirectFactory->create();

        //         return $resultRedirect->setPath('*/*/');
        //     }
        // }

        // $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        // if (!empty($data)) {
        //     $model->setData($data);
        // }

        // $this->_coreRegistry->register('storelocator_storelocator', $model);

        // /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        // $resultPage->addBreadcrumb(
        //     $id ? __('Edit Store') : __('New Store'),
        //     $id ? __('Edit Store') : __('New Store')
        // );
        // $resultPage->getConfig()->getTitle()->prepend(__('Stores'));
        $resultPage->getConfig()->getTitle()->prepend(__('Import Stores'));

        return $resultPage;
    }
}
