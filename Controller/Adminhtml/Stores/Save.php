<?php
namespace Oliverbode\Storelocator\Controller\Adminhtml\Stores;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Save extends \Magento\Backend\App\Action
{
    /**
     * imageHelper factory
     *
     * @var \Oliverbode\Storelocator\Helper\Data
     */
    protected $imageHelper;

    /**
     * @param Action\Context $context
     */
    public function __construct(
        Action\Context $context,
        \Oliverbode\Storelocator\Helper\Data $imageHelper
    )
    {
        parent::__construct($context);
        $this->imageHelper = $imageHelper;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Oliverbode_Storelocator::save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /**
         * @todo check store value is correct
         */
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Oliverbode\Storelocator\Model\Storelocator $model */
            $model = $this->_objectManager->create('Oliverbode\Storelocator\Model\Storelocator');

            if (!$data['lat'] || !$data['lat']) {
                $address = $data['address'] . ', ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['postcode'] . ' ' . $data['country'];
                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
                $geo = json_decode($geo, true);
                if ($geo['status'] == 'OK') {
                  $data['lat'] = $geo['results'][0]['geometry']['location']['lat'];
                  $data['lng'] = $geo['results'][0]['geometry']['location']['lng'];
                }
            }

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

            // save image data and remove from data array
            if (isset($data['image'])) {
                $imageData = $data['image'];
                unset($data['image']);
            } else {
                $imageData = array();
            }


            $model->setData($data);

            $this->_eventManager->dispatch(
                'storelocator_storelocator_prepare_save',
                ['storelocator' => $model, 'request' => $this->getRequest()]
            );

            try {

                // $imageHelper = $this->_objectManager->get('Oliverbode\Storelocator\Helper\Data');

                if (isset($imageData['delete']) && $imageData['value']) {
                    $this->imageHelper->removeImage($imageData['value']);
                    $model->setImage('');
                }
                
                $imageFile = $this->imageHelper->uploadImage('image');
                if ($imageFile) {
                    $model->setImage($imageFile);
                }

                $model->save();

                $this->messageManager->addSuccess(__('You saved this Store.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the store.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
