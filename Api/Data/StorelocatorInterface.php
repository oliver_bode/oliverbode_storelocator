<?php
namespace Oliverbode\Storelocator\Api\Data;

interface StorelocatorInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID            = 'id';
    const STORE_TITLE   = 'store_title';
    const ADDRESS       = 'address';
    const CITY          = 'city';
    const STATE         = 'state';
    const POSTCODE      = 'postcode';
    const COUNTRY       = 'country';
    const PHONE         = 'phone';
    const EMAIL         = 'email';
    const LAT           = 'lat';
    const LNG           = 'lng';
    const IMAGE         = 'image';
    const CONTENT       = 'content';
    const IS_ENABLE     = 'is_enable';

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get store_title
     *
     * @return string|null
     */
    public function getStoreTitle();

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress();

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Get state
     *
     * @return string|null
     */
    public function getState();

    /**
     * Get postcode
     *
     * @return string|null
     */
    public function getPostcode();

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Get phone
     *
     * @return string|null
     */
    public function getPhone();

    /**
     * Get email
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Get lat
     *
     * @return string|null
     */
    public function getLat();

    /**
     * Get lng
     *
     * @return string|null
     */
    public function getLng();

    /**
     * Get image
     *
     * @return string|null
     */
    public function getImage();

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get is_enable
     *
     * @return string|null
     */
    public function getIsEnable();



    /**
     * Set ID
     *
     * @param int $id
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setId($id);

    /**
     * Set store_title
     *
     * @param string $storeTitle
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setStoreTitle($id);

    /**
     * Set address
     *
     * @param string $address
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setAddress($address);

    /**
     * Set city
     *
     * @param string $city
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setCity($city);

    /**
     * Set state
     *
     * @param string $state
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setState($state);

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setPostcode($postcode);

    /**
     * Set country
     *
     * @param string $country
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setCountry($country);

    /**
     * Set phone
     *
     * @param string $phone
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setPhone($phone);

    /**
     * Set email
     *
     * @param string $email
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setEmail($email);

    /**
     * Set lat
     *
     * @param string $lat
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setLat($lat);

    /**
     * Set lng
     *
     * @param string $lng
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setLng($lng);

    /**
     * Set image
     *
     * @param string $image
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setImage($image);

    /**
     * Set content
     *
     * @param string $content
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setContent($content);

    /**
     * Set is_enable
     *
     * @param int|bool $isEnable
     * @return \Oliverbode\Storelocator\Api\Data\StorelocatorInterface
     */
    public function setIsEnable($isEnable);
}
